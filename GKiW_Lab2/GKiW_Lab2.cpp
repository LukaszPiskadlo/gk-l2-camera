﻿#include "stdafx.h"
#include "GKiW_Lab2.h"

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);

    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640, 360);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("GKiW: Lab 2");

    glutDisplayFunc(OnRender);
    glutReshapeFunc(OnReshape);
    glutKeyboardFunc(OnKeyPress);
    glutKeyboardUpFunc(OnKeyUp);
    glutPassiveMotionFunc(OnMouseMove);
    glutTimerFunc(17, OnTimer, 0);

    glEnable(GL_DEPTH_TEST);
    glutSetCursor(GLUT_CURSOR_NONE);
    glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);

    // Inicjalizacja stanu kamery:
    player.pos.x = 0.0f;
    player.pos.y = 1.75f;
    player.pos.z = 0.0f;
    player.dir.x = 0.0f;
    player.dir.y = 0.0f;
    player.dir.z = -1.0f;
    player.speed = 0.1f;
    player.inertiaX = 0.0f;
    player.inertiaZ = 0.0f;
    player.inertiaAngleXZ = 0.0f;
    player.inertiaAngleY = 0.0f;
    player.mouseX = 0;
    player.mouseY = 0;

    glutMainLoop();

    return 0;
}

// Tablica przechowująca stan klawiszy w formie flag, indeksowana wg kodów ASCII.
bool keystate[256];

// Obsługa zdarzenia, gdy zostanie wciśnięty klawisz - zdarzenie nieoodporne na repetycję klawiszy.
void OnKeyPress(unsigned char key, int x, int y) {
    //printf("KeyPress: %c\n", key);
    if (!keystate[key]) {
        OnKeyDown(key, x, y); // Emulacja zdarzenia związanego z pojedynczym wciśnięciem klawisza
    }
    keystate[key] = true;
}

// Obsługa naszego własnego zdarzenia, gdy zostanie po raz pierwszy wciśnięty klawisz - zdarzenie odporne na repetycję.
void OnKeyDown(unsigned char key, int x, int y) {
    //printf("KeyDown: %c\n", key);
    if (key == 27) { // ESC - wyjście
        glutLeaveMainLoop();
    }
}

// Obsługa zdarzenia puszczenia klawisza.
void OnKeyUp(unsigned char key, int x, int y) {
    printf("KeyUp: %c\n", key);
    keystate[key] = false;
}

// Aktualizacja stanu gry - wywoływana za pośrednictwem zdarzenia-timera.
void OnTimer(int id) {

    // Chcemy, by ta funkcja została wywołana ponownie za 17ms.
    glutTimerFunc(17, OnTimer, 0);

    int centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
    int centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

    if (player.mouseX != centerX || player.mouseY != centerY)
    {
        int diffX = player.mouseX - centerX;
        int diffY = player.mouseY - centerY;
        player.inertiaAngleXZ = diffX * player.speed * 0.1f;
        player.inertiaAngleY = diffY * player.speed * 0.1f;

        glutWarpPointer(centerX, centerY);
    }

    // Chodzenie do przodu:
    if (keystate['w'])
        player.inertiaX = player.speed;

    // Chodzenie do tyłu:
    if (keystate['s'])
        player.inertiaX = -player.speed;

    if (keystate['a'])
        player.inertiaZ = -player.speed;

    if (keystate['d'])
        player.inertiaZ = player.speed;

    if (keystate['q'])
        player.inertiaAngleXZ = -player.speed / 3.0f;

    if (keystate['e'])
        player.inertiaAngleXZ = player.speed / 3.0f;

    if (keystate['r'])
        player.inertiaAngleY = -player.speed / 3.0f;

    if (keystate['f'])
        player.inertiaAngleY = player.speed / 3.0f;

    if (fabs(player.inertiaX) > 0.005f)
    {
        player.pos.x += player.dir.x * player.inertiaX;
        player.pos.y += player.dir.y * player.inertiaX;
        player.pos.z += player.dir.z * player.inertiaX;
        player.inertiaX *= 0.85f;
    }

    if (fabs(player.inertiaZ) > 0.005f)
    {
        player.pos.x += -player.dir.z * player.inertiaZ;
        player.pos.z += player.dir.x * player.inertiaZ;
        player.inertiaZ *= 0.85f;
    }

    float theta = acosf(player.dir.y);
    float phi = atan2f(player.dir.z, player.dir.x);

    if (fabs(player.inertiaAngleXZ) > 0.005f || fabs(player.inertiaAngleY) > 0.005f)
    {
        player.dir.x = sinf(theta) * cosf(phi + player.inertiaAngleXZ);
        player.dir.z = sinf(theta) * sinf(phi + player.inertiaAngleXZ);
        player.dir.y = cosf(theta + player.inertiaAngleY);
        player.inertiaAngleXZ *= 0.9f;
        player.inertiaAngleY *= 0.9f;
    }
}

void OnMouseMove(int x, int y)
{
    player.mouseX = x;
    player.mouseY = y;
}

void OnRender() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Ustawienie kamery na podstawie jej stanu przechowywanego w zmiennej player.
    gluLookAt(
        player.pos.x, player.pos.y, player.pos.z, // Pozycja kamery
        player.pos.x + player.dir.x, player.pos.y + player.dir.y, player.pos.z + player.dir.z, // Punkt na ktory patrzy kamera (pozycja + kierunek)
        0.0f, 1.0f, 0.0f // Wektor wyznaczajacy pion
    );

    // Narysowanie "siatki" złożonej ze 121 kolorowych sfer.
    for (int ix = -5; ix <= 5; ix += 1) {
        for (int iz = -5; iz <= 5; iz += 1) {
            glColor3f(.5f + .1f * ix, .5f - .1f * iz, 0.0f);
            glPushMatrix();
            glTranslatef(ix, 1.0f, iz);
            glutSolidSphere(.05f, 8, 8);
            glPopMatrix();
        }
    }

    glFlush();
    glutSwapBuffers();
    glutPostRedisplay();

}

void OnReshape(int width, int height) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    gluPerspective(60.0f, (float)width / height, .01f, 100.0f);
}
